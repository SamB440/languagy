Languagy
=============
Languagy is a Spigot API for providing player-individual languages for plugins. It supports all languages that the Minecraft client supports.
<hr>

Spigot: https://www.spigotmc.org/resources/languagy-1-9-1-15.61663/
<br>
Wiki: https://fortitude.islandearth.net

<hr>